// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import moment from 'moment'

Vue.use(BootstrapVue)
Vue.use(VueResource)

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).calendar(null, {
      sameDay: '[Сегодня] в H:mm',
      nextDay: '[Завтра] в H:mm',
      nextWeek: 'dddd в H:mm',
      lastDay: '[Вчера] в H:mm',
      lastWeek: 'dddd в H:mm',
      sameElse: 'DD.MM.YY в H:mm'
    })
  }
})

moment.updateLocale('en', {
  weekdays: [
    'в воскресенье',
    'в понедельник',
    'во вторник',
    'в среду',
    'в четверг',
    'в пятницу',
    'в субботу'
  ]
})

Vue.http.options.emulateJSON = true
Vue.http.options.xhr = {withCredentials: true}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
