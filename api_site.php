<?php
// array holding allowed Origin domains
$allowedOrigins = array(
  '(http(s)://)?(www\.)?my\-domain\.com'
);
 
if (isset($_SERVER['HTTP_ORIGIN']) && $_SERVER['HTTP_ORIGIN'] != '') {
  foreach ($allowedOrigins as $allowedOrigin) {
    if (preg_match('#' . $allowedOrigin . '#', $_SERVER['HTTP_ORIGIN'])) {
      header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
      header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
      header('Access-Control-Max-Age: 1000');
      header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
      break;
    }
  }
}
header('Content-Type: application/json');
$api = $_SERVER['SERVER_NAME'].'/api_default.php';
$api_key = 'e1b5d95dede8ce565b38ba8d4fbf0cd4';
include '../library/config.php';

$db_host = $config['db']['host'];
$db_name = $config['db']['dbname'];
$db_user = $config['db']['username'];
$db_pass = $config['db']['password'];

$db = mysqli_connect($db_host,$db_user,$db_pass,$db_name) or die();
$res = mysqli_query($db,"set names utf8");

switch ($_GET['action']) {
    case 'authenticate':
        $token_json = curl($api.'?action=authenticate&username='.$_GET["username"].'&password='.$_GET["password"]);
        $token_decode = json_decode($token_json,1);
        $token = $token_decode['hash'];
        if (!$token) {
            die ();
        } else {
            die ($token_json);
        }
        break;
    case 'getUser':
        $users_json = curl($api.'?action=getUser&value='.$_GET["value"].'&hash='.$_GET["hash"]);
        die ($users_json);
        break;
    case 'getUserS':
        $users_json_s = curl($api.'?action=getUser&value='.$_GET["value"].'&hash='.$api_key);
        die ($users_json_s);
        break;
    case 'register':
        die (curl($api.'?action=register&hash='.$api_key.'&username='.$_GET["username"].'&password='.$_GET["password"].'&email='.$_GET["email"]));
        break;
    case 'getAvatar':
        $users_json = curl($api.'?action=getAvatar&value='.$_GET["value"].'&hash='.$_GET["hash"].'&size='.$_GET["size"]);
        die ($users_json);
        break;
    case 'getGroup':
        $users_json = curl($api.'?action=getGroup&value='.$_GET["value"].'&hash='.$_GET["hash"]);
        die ($users_json);
        break;
    case 'getDateUpgrade':
        $res = mysqli_query($db,"SELECT * FROM xf_user_upgrade_active WHERE user_id='".$_GET['value']."' and (user_upgrade_id = '8' or user_upgrade_id = '10' or user_upgrade_id = '16')");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($users_json);
        break;
    case 'getPosts':
        $users_json = curl($api.'?action=getPosts&hash='.$_GET["hash"].'&node_id=122');
        die ($users_json);
        break;
    case 'getHeroes':
        $res = mysqli_query($db,"SELECT * FROM a_heroes WHERE 1");
        $counts = mysqli_num_rows($res);
        while ($r = mysqli_fetch_assoc($res)) {
            $heroes[] = $r;
        }
        $result = array('count' => $counts, 'heroes' => $heroes);
        die (json_encode($result));
        break;
    case 'setHeroScript':
        $res = mysqli_query($db,"INSERT into a_hero_scripts (hero_id, hero_name, post_id, github_link, image_src, script_name, file_url) values ('".$_GET['hero_id']."', '".$_GET['hero_name']."', '".$_GET['post_id']."', '".$_GET['github_link']."', '".$_GET['image_src']."', '".$_GET['script_name']."', '".$_GET['file_url']."')");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'editHeroScript':
        $res = mysqli_query($db,"UPDATE a_hero_scripts SET hero_id = '".$_GET['hero_id']."', hero_name = '".$_GET['hero_name']."', script_name = '".$_GET['script_name']."', post_id = '".$_GET['post_id']."', github_link = '".$_GET['github_link']."', image_src = '".$_GET['image_src']."' ,file_url = '".$_GET['file_url']."' WHERE hero_script_id = '".$_GET['script_id']."'");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'deleteHeroScript':
        $res = mysqli_query($db,"DELETE FROM a_hero_scripts WHERE hero_script_id = '".$_GET['script_id']."'");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'getHeroeScripts':
        $res_hero_scripts = mysqli_query($db,"SELECT * FROM a_hero_scripts ORDER BY post_id");
        $count_scripts = mysqli_num_rows($res_hero_scripts);
        while ($hero_script = mysqli_fetch_assoc($res_hero_scripts)) {
            $hero_scripts[] = $hero_script;
            $heros[] = $hero_script['hero_id'];
        }
        $unique = array_keys(array_flip($heros));
        for ($j = 0; $j < $count_scripts; $j++) {
            $where_thread .= " or thread_id = ".$hero_scripts[$j]['post_id'];
        }
        
        $res_threads = mysqli_query($db,"SELECT first_post_id FROM xf_thread WHERE thread_id = 999999$where_thread");
        while ($thread = mysqli_fetch_assoc($res_threads)) {
            $threads[] = $thread;
        }
        
        for ($k = 0; $k < $count_scripts; $k++) {
            $where_post .= " or post_id = ".$threads[$k]['first_post_id'];
        }
        
        $res_posts = mysqli_query($db,"SELECT message FROM xf_post WHERE post_id = 9874561$where_post");
        while ($post = mysqli_fetch_assoc($res_posts)) {
            $posts[] = $post;
        }
        
        for ($u = 0; $u < $count_scripts; $u++) {
            $hero_scripts[$u]['message'] = $posts[$u]['message'];
        }
        
        $result = array('count' => $count_scripts, 'scripts' => by_hero_id($hero_scripts), 'heroes' => $unique);
        print_r (json_encode($result));
        break;
    case 'getPost':
        $thread = curl($api.'?action=getThread&value='.$_GET["id"].'&hash='.$_GET["api_key"]);
        $thread_arr = json_decode($thread);
        $post_id = $thread_arr->first_post_id;
        $users_json = curl($api.'?action=getPost&value='.$post_id.'&hash='.$_GET["api_key"]);
        die ($users_json);
        break;
    case 'setScript':
        $res = mysqli_query($db,"INSERT into a_scripts (post_id, github_link, image_src, script_name, file_url) values ('".$_GET['post_id']."', '".$_GET['github_link']."', '".$_GET['image_src']."', '".$_GET['script_name']."', '".$_GET['file_url']."')");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'getScripts':
        $res_scripts = mysqli_query($db,"SELECT * FROM a_scripts ORDER BY post_id");
        $count_scripts = mysqli_num_rows($res_scripts);
        while ($script = mysqli_fetch_assoc($res_scripts)) {
            $scripts[] = $script;
        }
        for ($j = 0; $j < $count_scripts; $j++) {
            $where_thread .= " or thread_id = ".$scripts[$j]['post_id'];
        }
        
        $res_threads = mysqli_query($db,"SELECT first_post_id FROM xf_thread WHERE thread_id = 999999$where_thread");
        while ($thread = mysqli_fetch_assoc($res_threads)) {
            $threads[] = $thread;
        }
        
        for ($k = 0; $k < $count_scripts; $k++) {
            $where_post .= " or post_id = ".$threads[$k]['first_post_id'];
        }
        
        $res_posts = mysqli_query($db,"SELECT message FROM xf_post WHERE post_id = 9874561$where_post");
        while ($post = mysqli_fetch_assoc($res_posts)) {
            $posts[] = $post;
        }
        
        for ($u = 0; $u < $count_scripts; $u++) {
            $scripts[$u]['message'] = $posts[$u]['message'];
        }
        
        $result = array('count' => $count_scripts, 'scripts' => $scripts);
        print_r (json_encode($result));
        break;
    case 'deleteScript':
        $res = mysqli_query($db,"DELETE FROM a_scripts WHERE script_id = '".$_GET['script_id']."'");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'editScript':
        $res = mysqli_query($db,"UPDATE a_scripts SET script_name = '".$_GET['script_name']."', post_id = '".$_GET['post_id']."', github_link = '".$_GET['github_link']."', image_src = '".$_GET['image_src']."', file_url = '".$_GET['file_url']."' WHERE script_id = '".$_GET['script_id']."'");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'editStatus':
        $res = mysqli_query($db,"UPDATE a_settings SET status_cheat = '".$_GET['status_cheat']."', status_color = '".$_GET['status_color']."' WHERE 1");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'editLink':
        $res = mysqli_query($db,"UPDATE a_settings SET link_to_down = '".$_GET['link_to_down']."', link_to_down_text = '".$_GET['link_to_down_text']."' WHERE 1");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'getSettings':
        $res_scripts = mysqli_query($db,"SELECT * FROM a_settings WHERE 1");
        $script = mysqli_fetch_assoc($res_scripts);
        
        
        print_r (json_encode($script));
        break;
    
    case 'setFaq':
        $res = mysqli_query($db,"INSERT into a_faq (question, answer) values ('".$_GET['question']."', '".$_GET['answer']."')");
        $arruser = mysqli_fetch_assoc($res);
        $users_json = json_encode($arruser);
        die ($res);
        break;
    case 'getFaq':
        $res_faq = mysqli_query($db,"SELECT * FROM a_faq WHERE 1");
        $count_faq = mysqli_num_rows($res_faq);
        while ($faq = mysqli_fetch_assoc($res_faq)) {
            $faqs[] = $faq;
        }
        
        $result = array('count' => $count_faq, 'faq' => $faqs);
        print_r (json_encode($result));
        break;
    case 'deleteFaq':
        $res = mysqli_query($db,"DELETE FROM a_faq WHERE faq_id = '".$_GET['faq_id']."'");
        
        die ($res);
        break;
    case 'editFaq':
        $res = mysqli_query($db,"UPDATE a_faq SET question = '".$_GET['question']."', answer = '".$_GET['answer']."' WHERE faq_id = '".$_GET['faq_id']."'");
        
        die ($res);
        break;
        
    case 'getHWIDdate':
        $user_id = $_GET['user_id'];
        $hwid = $_GET['hwid'];
        $res_HWID_date = mysqli_query($db,"SELECT edit_date FROM xf_user_change_log WHERE user_id = $user_id AND field = 'custom_fields:hwid' AND new_value = '$hwid'");
        $count_HWID_date = mysqli_num_rows($res_HWID_date);
        while ($HWID_date = mysqli_fetch_assoc($res_HWID_date)) {
            $HWID_dates[] = $HWID_date;
        }
        
        $result = array('count' => $count_HWID_date, 'HWID_date' => $HWID_dates);
        die (json_encode($result));
        break;
    case 'resetHWID':
        $set_hwid_json = curl($api.'?action=editUser&hash='.$api_key.'&user='.$_GET["user_id"].'&custom_fields=hwid=');
        die ($set_hwid_json);
    case 'userUpgrade':
        $user_id = $_GET['us_user_id'];
        $user_upgrade_id = $_GET['us_upgrade_id'];
        if ($user_upgrade_id == '8') {$length_amount = 1; $length_unit = 'month';$end_date = $_GET['us_time']+(30*24*60*60);}
        if ($user_upgrade_id == '10') {$length_amount = 7; $length_unit = 'day';$end_date = $_GET['us_time']+(7*24*60*60);}
        if ($user_upgrade_id == '16') {$length_amount = 0; $length_unit = '';$end_date = 0;}
        if ($user_upgrade_id == '14') {$length_amount = 1; $length_unit = 'month';$end_date = $_GET['us_time']+(30*24*60*60);}
        $extra = serialize(array('cost_amount' => $_GET['amount'], 'cost_currency' => 'rur', 'length_amount' => $length_amount, 'length_unit' => $length_unit));
        $start_date = $_GET['us_time'];
        $res = mysqli_query($db,"INSERT into xf_user_upgrade_active (user_id, user_upgrade_id, extra, start_date, end_date) values ('".$user_id."', '".$user_upgrade_id."', '".$extra."', '".$start_date."', '".$end_date."')");
        $set_user_group = curl($api.'?action=editUser&hash='.$api_key.'&user='.$_GET["us_user_id"].'&add_groups=72');
        
        die (json_encode($res));
        break;
    case 'getUpgrade':
        $res = mysqli_query($db,"SELECT * FROM xf_user_upgrade WHERE user_upgrade_id = 8 or user_upgrade_id = 10 or user_upgrade_id = 16");
        while ($Upgrade = mysqli_fetch_assoc($res)) {
            $Upgrades[] = $Upgrade;
        }
        die (json_encode($Upgrades));
        break;
}

function by_hero_id($arr) {
  $result = array();
  foreach ($arr as $l) {
    $result[$l['hero_id']][] = [
    'script_name' => $l['script_name'],
    'hero_script_id' => $l['hero_script_id'],
    'hero_name' => $l['hero_name'],
    'script_name' => $l['script_name'],
    'post_id' => $l['post_id'],
    'github_link' => $l['github_link'],
    'image_src' => $l['image_src'],
    'hero_id' => $l['hero_id'],
    'file_url' => $l['file_url'],
    'message' => $l['message']
];
  }
  return $result;
}

function curl( $url ){
    $ch = curl_init( $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    $response = curl_exec( $ch );
    curl_close( $ch );
    return $response;
}
?>